# Lambda-TS-Demo
This is a demo project indicating an issue with webpack and lambda when using the snowflake-sdk repository.

## Setting up
Before doing anything make sure you have the SAM cli installed. You don't need to setup AWS credentials as you can use the SAM cli to mimic AWS lambda locally using docker. But this means that you also need docker installed

## Building and running
To build the code run `make build` - This will build the TS into JS . You can then `make run` to run the code locally.

## Reproducing the error
If you are on the main branch then everything will work fine when you run the project:

![main image run](./main.png)

Then to reproduce the issue, delete your .aws-sam folder in the project root, switch to the branch "broken" and redo the steps to build and run. This time, the handler will time out:

![broken image run](./broken.png)