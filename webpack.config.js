const path = require("path");
const AwsSamPlugin = require("aws-sam-webpack-plugin");
const awsSamPlugin = new AwsSamPlugin();

module.exports = {
    entry: () => awsSamPlugin.entry(),
    output: {
        filename: (chunkData) => awsSamPlugin.filename(chunkData),
        libraryTarget: "commonjs",
        path: path.resolve(".")
    },
    devtool: process.env.RELEASE_BUILD ? false : "source-map",
    resolve: {
        extensions: [".ts", ".js"]
    },
    target: "node",
    mode: process.env.RELEASE_BUILD ? "production" : "development",
    module: {
        rules: [{ test: /\.ts?$/, loader: "ts-loader" }]
    },
    plugins: [awsSamPlugin]
};