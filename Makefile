# AWS
AWS_PROFILE ?= default
AWS_REGION ?= eu-west-1

# Stack information
MAIN_STACK_NAME = snowflake-demo-stack

build: build_ts
run: 
	sam local invoke "Snowflake"
	
build_ts:
	webpack-cli

deploy:
	sam deploy \
			--profile $(AWS_PROFILE) \
			--region $(AWS_REGION) \
			--stack-name $(MAIN_STACK_NAME) \
			--capabilities CAPABILITY_IAM CAPABILITY_AUTO_EXPAND \
			--resolve-s3 \
			--no-fail-on-empty-changeset \
			--no-confirm-changeset \
			--no-fail-on-empty-changeset

delete_stack:
	sam delete \
		--stack-name $(STACK_NAME) \
		--profile $(AWS_PROFILE) \
		--region $(AWS_REGION) \
		--no-prompts